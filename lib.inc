%define space_symbol 0x20
%define newline_symbol 0xA
%define tab_symbol 0x9

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, 60
	syscall
	
 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
 xor rax, rax	; очищаем rax, чтобы в нем гарантированно был нуль
.loop:
	cmp byte[rdi+rax], 0			
	je .end				
	inc rax				
	jmp .loop
.end:
	ret	


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi				
	mov rdx, rax				
	mov rsi, rdi				
	mov rdi, 1				 		
	mov rax, 1						
	syscall
	xor rax, rax
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi , newline_symbol
	

; Принимает код символа и выводит его в stdout
print_char:
    push rdi	
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    mov rsi, rsp
    syscall
    pop rdi
    ret



; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, rsp 				;в r9 адрес вершины стека
	mov rax, rdi 				;в rax число, которое нужно перевести
	mov rcx, 10  				;rcx в качестве делителя
	dec rsp;
	mov byte[rsp], 0
.loop:
	xor rdx, rdx
	div rcx 				
	add rdx, "0" 				
	dec rsp
	mov byte[rsp], dl 			
	test rax, rax 				
	jnz .loop

	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
	ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg 					
    jmp print_uint
     				

.neg:
    push rdi  					
    mov rdi, '-' 				
    call print_char
    pop rdi
    neg rdi 					
    jmp print_uint
     					
    



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi     			
    mov rcx, rax           			
    xchg rdi, rsi
    push rdi
    push rsi   
    push rcx      			
    call string_length
    pop rcx
    pop rsi
    pop rdi     			
    cmp rax, rcx           			
    jne .false             			
.loop:
    dec rcx               			
    mov rax, [rdi+rcx]    			;сохраняем значение последнего символа в rax
    cmp al, [rsi+rcx]     			
    jne .false            			
    cmp rcx, 0           			;проверяем если длина строки равна 0
    jg .loop             			;если нет - цикл
    mov rax, 1           			;в аккумулятор 1 так как строки равны  
    ret                 
.false:
    xor rax, rax           			
    ret
	
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, 0 				
	mov rdi, 0 				
	mov rdx, 1 				 
	push 0	  				
	mov rsi, rsp 				
	syscall
	pop rax	     			
	ret	

    
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	mov r8, rdi 				
	mov r9, rsi 				
	xor r10, r10 				

.first_read:
	push r8
	push r9
	push r10
	call read_char
	pop r10
	pop r9
	pop r8
	cmp rax, space_symbol 				
	je .first_read
	cmp rax, tab_symbol  				
	je .first_read
	cmp rax, newline_symbol  				
	je .first_read
	cmp rax, 0x0  				
	je .end
	
.continue_read:
	cmp r9, r10 				
	jbe .overflow
	mov byte[r8+r10], al 			
	cmp al, space_symbol
	je .end
	cmp al, tab_symbol
	je .end
	cmp al, newline_symbol
	je .end
	cmp al, 0x0
	je .end
	inc r10
	cmp r10, r9
	je .overflow
	push r8
	push r9
	push r10
	call read_char
	pop r10
	pop r9
	pop r8 
	jmp .continue_read

.overflow:
	xor rax, rax
	ret
.end:
	mov byte[r8+r10] , 0
	mov rax, r8
	mov rdx, r10
	ret	


		 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor r10, r10			
	mov rcx, 10				; 10 в rcx используем в качестве делителям
	xor rax, rax
.loop:
	xor r11, r11
	mov r11b, byte[rdi + r10]
	cmp r11b, "9"		
	ja .end			
	cmp r11b, "0"			
	jb .end					
	inc r10			
	sub r11b, '0'		
	mul rcx			
	add rax, r11		
	jmp .loop
.end:
	mov rdx, r10		
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
.check_number_sign:
        mov al, byte[rdi]
        cmp al, '-'            		; провекрка на знак минус
        jne parse_uint	
        inc rdi                		; если число отрицательное передвигаем указатель только на число
.is_it_digit: 					;проверка на число
        mov al, byte[rdi]
        cmp al, '0'
        jl .else
        cmp al, '9'
        jg .else
    	 push rdi
   	 call parse_uint 			;получаем числовое значение
   	 pop rdi
   	 cmp rdx,0
   	 je .else
   	 neg rax 				; меням знак полученного безнакового числа
   	 inc rdx 				; в rdx пришла длина безнакового числа, делаем инкремент, потому что в нашем случае есть "-"
   	 ret
 
.else:
        xor rax, rax
        xor rdx, rdx
        ret  


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi					;пушим указатель на строку
    push rsi					;пушим указатель на буфер	
    push rdx					;пушим длину буфера 	
    call string_length 			;string length in rax
    inc rax 					;cuz null terminator
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jl .fail 					;if rdx<rax then end
    push rax
    .loop:
        mov r8b, [rdi]
        mov [rsi], r8b
        inc rsi
        inc rdi
        dec rax
        jne .loop
    pop rax
    jmp .end
    .fail:
        xor rax, rax
    .end:
        ret


